# Laravel-titles
Slightly reworked version of [rephluX/laravel-pagetitle](https://github.com/rephluX/laravel-pagetitle) package.

## Installation
Type in terminal
```bash
composer require jes490/titles
```

### In Laravel < 5.5
Add Service Provider and Facade in `config.app`.

```php
'providers' => [
    '...',
    Jes490\Titles\TitlesServiceProvider::class
];
```

```php
 'aliases' => [
     '...'
     'Titles' => Jes490\Titles\Facades\Titles::class,
 ]
```

## Usage
You can add titles to the page like this:
```php
public function index()
{
    \Titles::add('Welcome to our Homepage');

    return view('hello');
}
```
Or using helper function:
```php
public function index()
{
    page_title('Welcome to our Homepage');

    return view('hello');
}
```
### Several arguments
You pass several arguments to `add()` method or `titles()` helper.
```php
public function index()
{
    page_title('Admin', 'Dashboard');           //several arguments
    page_title(['Admin', 'Dashboard']);         //you can pass array too
    page_title(['Admin', 'Dashboard'], 'Show'); //or combine both methods
}
```

## Render titles

Add titles to your view.
```html
<head>
    <meta charset="UTF-8">
    <title>{{ page_title()->get() }}</title> <!-- It will render title for your front-end page in regular order. !-->
    <title>{{ page_title()->get($order, $page) }}</title>
    <!-- Order: 'regular', 'reverse', 'prepend-reverse', 'append-reverse' !-->
    <!-- Page: 'front', 'backend' !-->
    ...
</head>
```

## Configuration

In laravel you can add `titles.php` config.

```bash
php artisan vendor:publish --provider="Jes490\Titles\TitlesServiceProvider"
```

You can change this:
```php
'front_name'          => ''     //default title for your front-page 
'backend_name'        => ''     //default title for your backend-page
'default_page_when_empty' => '' //default title when no title set
'delimeter'           => ' | '  //delimeter for your titles
```

## Change the configuration values

Each of the configuration parameters can be changed on the page title object instance with the correspondig setter methods:

* setDelimeter(delimeter)
* setPageName(pageName)
* setDefault(default)

Example usage:

```php
public function index()
{
    page_title('My Blog Post')->setPageName('My Blog')->setDelimeter(' / ');

    return view('hello');
}
```

To retrieve the current configuration values use the corresponding getter methods on the page title object instance:

* getDelimeter()
* getPageName()
* getDefault()
