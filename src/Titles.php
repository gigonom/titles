<?php

namespace Jes490\Titles;

use Countable;

/**
 * Class Titles
 *
 * @author jes490 <aseselkin@gmail.com>
 */
class Titles implements Countable
{
    /**
     * Collection of page title parts.
     *
     * @var array
     */
    protected $collection = [];

    /**
     * Delimeter string for seperate the page title parts.
     *
     * @var string
     */
    protected $delimeter;

    /**
     * The front page name to append or prepend to the page title parts.
     *
     * @var string
     */
    protected $frontName;

    /**
     * The backend page name to append or prepend to the page title parts.
     *
     * @var string
     */
    protected $backendName;

    /**
     * The default front page text when no page title is set.
     *
     * @var string
     */
    protected $default;

    /**
     * @param string $delimeter
     * @param string $frontName
     * @param string $backendName
     * @param string $default
     */
    public function __construct($delimeter = ' | ', $frontName = '', $backendName = '', $default = '')
    {
        $this->delimeter = $delimeter;
        $this->frontName = $frontName;
        $this->backendName = $backendName;
        $this->default   = $default;
    }

    /**
     * @return string
     */
    public function getDelimeter()
    {
        return $this->delimeter;
    }

    /**
     * @param string $delimeter
     *
     * @return $this
     */
    public function setDelimeter($delimeter)
    {
        $this->delimeter = $delimeter;

        return $this;
    }

    /**
     * @return string
     */
    public function getFrontPageName()
    {
        return $this->frontName;
    }

    /**
     * @param string $pageName
     *
     * @return $this
     */
    public function setFrontPageName($pageName)
    {
        $this->frontName = $pageName;

        return $this;
    }

    /**
     * @return string
     */
    public function getBackendPageName()
    {
        return $this->backendName;
    }

    /**
     * @param string $pageName
     *
     * @return $this
     */
    public function setBackendPageName($pageName)
    {
        $this->backendName = $pageName;

        return $this;
    }

    /**
     * @return string
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * @param string $default
     *
     * @return $this
     */
    public function setDefault($default)
    {
        $this->default = $default;

        return $this;
    }

    /**
     * Add items to collection.
     *
     * @param $items
     *
     * @return mixed
     */
    public function add(...$items)
    {
        foreach ($items as $item)
            $this->addItem($item);
    }

    /**
     * Add an item to the collection.
     *
     * @param $item
     *
     * @return mixed
     */
    public function addItem($item)
    {
        if (is_array($item)) {
            return array_map([$this, 'add'], $item);
        }

        if (! $item | strlen(trim($item)) === 0) {
            return false;
        }

        $this->collection[] = trim(strip_tags($item));
    }

    /**
     * Count the collection.
     *
     * @return int
     */
    public function count()
    {
        return count($this->collection);
    }

    /**
     * Get the page title.
     *
     * @param bool|string $direction
     * @param string $title
     *
     * @return string
     */
    public function get($direction = 'regular', $title = 'front')
    {
        if ($this->count() == 0) {
            return $this->default;
        }

        if ($direction === 'regular') {
            $this->collection = array_reverse($this->collection);
            $this->addPageName($title);
            $this->collection = array_reverse($this->collection);
        }

        if ($direction === 'reverse') {
            $this->collection = array_reverse($this->collection);
            $this->addPageName($title);
        }

        if ($direction === 'prepend-reverse') {
            $this->addPageName($title);
        }

        if ($direction === 'append-reverse') {
            $this->addPageName($title);
            $this->collection = array_reverse($this->collection);
        }

        return implode($this->getDelimeter(), $this->collection);
    }

    /**
     * Add the page name to the collection.
     *
     * @param string $title
     */
    protected function addPageName($title)
    {
        $pageName = $this->getPageName($title);
        if (! empty($pageName) && ! in_array($pageName, $this->collection)) {
            $this->add($pageName);
        }
    }

    /**
     * Get corresponding page name.
     *
     * @param $title
     * @return string
     */
    public function getPageName($title)
    {
        if ($title === 'front')
            return $this->getFrontPageName();
        if ($title === 'backend')
            return $this->getBackendPageName();
        return '';
    }
}

