<?php

namespace Jes490\Titles;

use Illuminate\Support\ServiceProvider;

/**
 * Class TitlesServiceProvider.
 *
 * @author jes490 <aseselkin@gmail.com>
 */
class TitlesServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->app->singleton('Titles', function () {
            $delimeter = config('titles.delimiter');
            $frontName = config('titles.front_name');
            $backendName = config('titles.backend_name');
            $default   = config('titles.default_title_when_empty');

            return new Titles($delimeter, $frontName, $backendName, $default);
        });
    }

    public function boot()
    {
        $configPath = __DIR__.'/config/config.php';

        $this->mergeConfigFrom($configPath, 'titles');
        $this->publishes([$configPath => config_path('titles.php')], 'config');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['Titles'];
    }
}

