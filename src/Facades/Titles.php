<?php

namespace Jes490\Titles\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class PageTitle.
 *
 * @author Chris van Daele <engine_no9@gmx.net>
 */
class Titles extends Facade
{
    /**
     * Name of the binding in the IoC container.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Titles';
    }
}
