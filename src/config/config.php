<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Front Page Name
    |--------------------------------------------------------------------------
    |
    | Type your page name for your website.
    | This will be used when there are more titles to concatenate with.
    |
    */
    'front_name' => '',

    /*
    |--------------------------------------------------------------------------
    | Backend Page Name
    |--------------------------------------------------------------------------
    |
    | Type your page name for your website.
    | This will be used when there are more titles to concatenate with.
    |
    */
    'backend_name' => '',

    /*
    |--------------------------------------------------------------------------
    | Default front page title when empty
    |--------------------------------------------------------------------------
    |
    | This will be used when there is no other title.
    | Mainly used for the home page of your website.
    |
    */
    'default_page_when_empty' => '',


    /*
    |--------------------------------------------------------------------------
    | Delimiter
    |--------------------------------------------------------------------------
    |
    | Titles will be concatenated using this delimiter.
    |
    */
    'delimiter' => ' | ',
];
