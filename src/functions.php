<?php

if (! function_exists('page_title')) {

    /**
     * @param array ...$titles
     * @return array|\Illuminate\Foundation\Application|\Jes490\Titles\Titles|mixed
     */
    function page_title(...$titles)
    {
        $class = app('Titles');
        if (! is_null($titles)) {
            $class->add(...$titles);
        }

        return $class;
    }
}
